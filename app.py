#!/Users/vlad/anaconda/envs/ms/bin/python
import telneter
from models import *
import shortcuts

account = telneter.Account("vlad", "amenib1")

kits = {
    'via_mts': ['m9-sw15', 'm9-sw00', 'm9-sw11', 'mr-sw3', 'mr-sw0']
}

tag = 358
name = 'tinkoff_%s' % tag
source_bundle = 212
target_bundle = 1073

shortcuts.path_finder(source_bundle, target_bundle, tag, name, account)

