import sys
from telneter import Account
from models import Device
import requests as r
import shortcuts


def put_vlan(*args):
    """
    usage: put_vlan <hostname> <tag> <name> <ports>
    """
    if len(args) < 4:
        print('Wrong args')
        return

    hostname = args[0]
    tag = args[1]
    name = args[2]
    ports = args[3:]

    print('Put vlan "%s [%s]" to %s on ports %s, tagged' % (name, tag, hostname, ' and '.join(ports)))

    if input('Confirm? ') == 'y':
        a = Account()
        d = Device(hostname, a)
        result = d.put_vlan(tag, name, ports)


def remove_vlan(*args):
    """
    usage: remove_vlan <hostname> <tag>
    """
    if len(args) < 2:
        print('Wrong args')
        return

    hostname = args[0]
    tag = args[1]

    print('Remove vlan %s from %s' % (tag, hostname))

    if input('Confirm? ') == 'y':
        a = Account()
        d = Device(hostname, a)
        result = d.remove_vlan(tag)


def spread_vlan(*args):
    """
    usage: spread_vlan <source> <target> <tag> <name> <exclude_hostnames>
    """
    if len(args) < 4:
        print('Wrong args, must be 4 or more')
        return

    source = args[0]
    target = args[1]
    tag = args[2]
    name = args[3]
    url = 'https://hostel.ihome.ru/path_finder/api/suggested_paths/?source_id=%s&target_id=%s' % (source, target)
    if len(args) > 4:
        exclude_hosts = args[4:]
        for host in exclude_hosts:
            url += '&exclude_netname=%s' % host

    data = r.get(url).json()

    print('STEP 1: Choosing path')
    print(' ', data['target'], data['source'])

    i = 0
    for path in data['paths']:
        print(' ', '%s:' % i, ' -> '.join(path))
        i += 1

    choice = 0
    try:
        choice = int(input('Choose path number: '))
    except ValueError:
        print('Aborted')
        quit(1)

    path = data['paths'][choice]
    print('  You choose %s', ' -> '.join(path))

    url = 'https://hostel.ihome.ru/path_finder/api/full_path/' \
          '?requested_path=%s&source_id=%s&target_id=%s' % ('+'.join(path),
                                                            source,
                                                            target)
    data = r.get(url).json()
    computed_path = data['computed_path']

    print('STEP 2: Computed path')
    for host in computed_path:
        print(host['device'])
        if host['type'] == 'switch':
            print(' - ports', ' and '.join(host['ports']))
        elif host['type'] == 'router':
            print(' -', host['ccc_params'])

asking_help = ['help', '--help', '-h', '?']

if __name__ == '__main__':

    if len(sys.argv) > 1:
        action = sys.argv[1]
        if action in globals():
            agent = globals().get(action)

            if len(sys.argv) > 2 and sys.argv[2] in asking_help:
                print(action, agent.__doc__)
            else:
                agent(*sys.argv[2:])

        else:
            print('%s: command not found' % action)
            quit(1)
