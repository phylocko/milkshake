import re
from cli import *
from telneter import Executor


class Device:
    def __init__(self, hostname, account, verbose=False):
        self.hostname = hostname
        self.verbose = verbose

        try:
            executor = self.login(hostname, account)
        except ValueError as e:
            raise e

        if executor.os == "EXOS":
            self.cli = ExosCli(executor, verbose=verbose)

        elif executor.os == "JUNOS":
            self.cli = JunosCli(executor, verbose=verbose)
        else:
            raise ValueError("Unknown device OS")

    def login(self, hostname, account):
        try:
            executor = Executor(hostname, account)
        except ValueError as e:
            raise e
        return executor

    def logout(self):
        self.cli.executor.close()

    def cmd(self, cmd):
        if self.verbose:
            print(cmd)
        return self.cli.executor.cmd(cmd)

    def vlan_has_ip(self, vlan):
        return self.cli.vlan_has_ip(vlan)

    def get_vlan(self, vlan_id):
        return self.cli.get_vlan(vlan_id)

    def get_vlan_list(self):
        return self.cli.get_vlan_list()

    def put_vlan(self, tag, name, ports):
        return self.cli.put_vlan(tag, name, ports)

    def remove_vlan(self, vlan_id):
        return self.cli.remove_vlan(vlan_id)

    def assign_vlan_ip(self, vlan, ip_address):
        return self.cli.assign_vlan_ip(vlan, ip_address)

    def remove_vlan_ip(self, vlan):
        return self.cli.remove_vlan_ip(vlan)

    def is_address_reachable(self, ip_address):
        return self.cli.is_address_reachable(ip_address)

    def make_ccc(self, tag, name, interface, remote_address):
        return self.cli.make_ccc(tag, name, interface, remote_address)

    def find_mac(self, mac_address):
        return self.cli.find_mac(mac_address)

    def get_vlan_ports(self, tag):
        return self.cli.get_vlan_ports(tag)


