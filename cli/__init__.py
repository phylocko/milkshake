import re
from entities import *


class JunosCli:
    regexps = {
        'interface': '(ae[0-9]{1,2}|(ge-[0-9\/]{5,10}|xe-[0-9\/]{5,10}|et-[0-9\/]{5,10}|fe-[0-9\/]{5,10}))',
        'ipnetwork': '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\/[0-9]{1,2}',
    }

    def __init__(self, executor, verbose=False):
        self.verbose = verbose
        self.executor = executor
        self.cmd('cli')

    def cmd(self, command):
        result = self.executor.cmd(command)
        if self.verbose:
            print(result)
        return result

    def get_vlan_by_name(self, vlan_name):
        cmd = "show vlans %s" % vlan_name
        result = self.cmd(cmd)
        lenght = len(vlan_name)
        for l in result.splitlines():
            if l[0:lenght] == vlan_name:
                parts = l.split()
                vlan_id = int(parts[-1])
                return self.get_vlan(vlan_id)

    def get_interface(self, name):
        name = name.strip()
        if name[-2:] == ".0":
            name = name[0:-2]
        cmd = "show interfaces %s descriptions" % name
        result = self.cmd(cmd)
        lenght = len(name)
        for l in result.splitlines():
            if l[0:lenght] == name:
                description = l.split()[3]
                interface = Interface(name=name, description=description)
                return interface

    def get_vlan(self, vlan_id: int):
        cmd = 'show configuration vlans | match %s | display set' % vlan_id
        result = self.cmd(cmd)
        for l in result.splitlines():
            if 'set vlans' in l:
                parts = l.split()
                return Vlan(tag=vlan_id, name=parts[2])
        return None

    def get_vlan_list(self):
        dump = self.cmd('show vlans')
        vlans = []

        pattern = re.compile('^[a-zA-Z0-9-_]{1,20}\ +[0-9]{1,4}')
        for l in dump.splitlines():
            mathes = pattern.findall(l)
            for match in mathes:
                parts = match.split()
                vlan = Vlan(tag=int(parts[1]), name=parts[0])
                vlans.append(vlan)

        pattern = re.compile('^default-switch\ +[a-zA-Z0-9-_\.]*\ +[0-9]{1,4}')
        for l in dump.splitlines():
            mathes = pattern.findall(l)
            for match in mathes:
                parts = match.split()
                print(parts)
                vlan = Vlan(tag=int(parts[2]), name=parts[1])
                vlans.append(vlan)

        return vlans

    def put_vlan(self, tag, name, ports):
        self.cmd('configure')
        self.cmd('set vlans %s vlan-id %s' % (name, tag))
        for port in ports:
            self.cmd('set interfaces %s unit 0 family ethernet-switching vlan members %s' % (port, name))
        return self.commit()

    def remove_vlan(self, vlan_id):
        vlan = self.get_vlan(vlan_id)
        ports = self.get_vlan_ports(vlan_id)
        self.cmd('configure')
        for port in ports:
            self.cmd('delete interfaces %s unit 0 family ethernet-switching vlan members %s' % (port, vlan.name))
        self.cmd('delete vlans %s' % vlan.name)
        return self.commit()

    def remove_vlan_ip(self, vlan):
        print('JunOS cli can\'t remove vlan IP. Do it manually on %s', self.executor.hostname)

    def get_vlan_ports(self, tag):
        command = "show vlans %s detail" % tag
        pattern = re.compile(self.regexps['interface'])
        dump = self.cmd(command)
        interfaces = []
        for interface, _ in pattern.findall(dump):
            interfaces.append(interface)
        return interfaces

    def make_ccc(self, tag, name, interface, remote_address):
        cmd = 'op ccc-setup vlan-id {tag} description {name} remote-address {remote_address} interface {interface}' \
            .format(tag=tag, name=name, interface=interface, remote_address=remote_address)
        print(self.executor.hostname, cmd)

    def find_mac(self, mac_address):
        cmd = "show ethernet-switching table brief | match %s" % mac_address
        self.cmd("cli")
        mac_addresses = []
        result = self.cmd(cmd)

        for l in result.splitlines():
            if str(mac_address) in l:
                parts = l.split()
                if parts[1] == mac_address:
                    vlan = self.get_vlan_by_name(parts[0])
                    interface = self.get_interface(parts[-1])
                    mac = Mac(address=mac_address, interface=interface, vlan_id=vlan.tag, vlan_name=vlan.name)
                    mac_addresses.append(mac)

        return mac_addresses

    def vlan_has_ip(self, vlan):
        dump = self.cmd('show interfaces vlan.%s terse' % vlan.tag)
        pattern = re.compile(self.regexps['ipnetwork'])
        matches = pattern.findall(dump)
        if matches:
            return True
        return False

    def assign_vlan_ip(self, vlan, ip_address):
        self.cmd('configure')
        result = self.cmd('set interfaces vlan unit %s family inet address %s' % (vlan.tag, ip_address))
        result = self.cmd('set vlans %s l3-interface vlan.%s' % (vlan.name, vlan.tag))
        return self.commit()

    def is_address_reachable(self, ip_address):
        cmd = "ping %s count 4 wait 1 " % ip_address
        result = self.cmd(cmd)
        if "bytes from %s: icmp_seq" % ip_address in result:
            return True
        return False

    def commit(self):
        result = self.cmd('commit and-quit')
        if 'commit complete' in result:
            return True
        else:
            raise ValueError('Unable to configure vlan on %s' % self.executor.hostname)


class ExosCli:
    regexps = {
        'ipnetwork': '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\/[0-9]{1,2}',
    }

    def __init__(self, executor, verbose=False):
        self.executor = executor
        self.verbose = verbose

    def cmd(self, command):
        result = self.executor.cmd(command)
        if self.verbose:
            print(result)
        return result

    def get_interface(self, interface_name):
        cmd = "show ports %s no-refresh | i %s" % (interface_name, interface_name)
        interface_dump = self.cmd(cmd)
        for l in interface_dump.splitlines():
            lenght = len(interface_name)
            if l[0:lenght] == interface_name:
                interface_parts = l.split()
                description = interface_parts[1]
                interface = Interface(name=interface_name, description=description)
                return interface

    def get_vlan(self, tag):
        cmd = "show vlan tag %s" % tag
        result = self.cmd(cmd)
        for l in result.splitlines():
            if "VLAN Interface with" in l:
                vlan_name = l.split()[4]
                vlan = Vlan()
                vlan.tag = tag
                vlan.name = vlan_name
                vlan.raw = result
                return vlan

        raise ValueError("%s has no vlan %s" % (self.executor.hostname, tag))

    def get_vlan_list(self):
        dump = self.cmd('show vlan')
        pattern = re.compile('^[a-zA-Z0-9-_]{1,20}\ *[0-9]{1,4}')
        vlans = []
        for l in dump.splitlines():
            match = pattern.findall(l)
            if match:
                parts = match[0].split()
                vlan = Vlan(tag=int(parts[1]), name=parts[0])
                if vlan.tag > 1:
                    vlans.append(vlan)
        return vlans

    def put_vlan(self, tag, name, ports):
        self.cmd('create vlan %s tag %s' % (name, tag))
        for port in ports:
            result = self.cmd('configure vlan %s add ports %s tagged\r\ny' % (name, port))

    def remove_vlan(self, vlan_id):
        vlan = self.get_vlan(vlan_id)
        self.cmd('delete vlan %s' % vlan.name)

    def get_vlan_ports(self, tag):
        command = "show vlan tag %s" % tag
        port_pattern = re.compile('[0-9]{1,2}')
        brackets_pattern = re.compile('\([a-zA-Z0-9-_]*\)')

        dump = self.cmd(command)
        interfaces = []
        for l in dump.splitlines():
            if "Tag:" in l or "Untag:" in l:
                brackets = brackets_pattern.findall(l)
                if brackets:
                    for bracket in brackets:
                        l = l.replace(bracket, '')

                for interface in port_pattern.findall(l):
                    interfaces.append(interface)
        return interfaces

    def assign_vlan_ip(self, vlan, ip_address):
        cmd = "configure vlan %s ipaddress %s" % (vlan.name, ip_address)
        result = self.cmd(cmd)
        return "has been created" in result

    def remove_vlan_ip(self, vlan):
        cmd = "unconfigure vlan %s ipaddress" % vlan.name
        result = self.cmd(cmd)
        if "does not have primary address" in result:
            print('Vlan doesn\'t have IP address.')
        else:
            return True

    def vlan_has_ip(self, vlan):
        dump = self.cmd('show vlan tag %s | include Primary' % vlan.tag)
        pattern = re.compile(self.regexps['ipnetwork'])
        matches = pattern.findall(dump)
        if matches:
            return True
        return False

    def is_address_reachable(self, ip_address):
        cmd = "ping count 4 %s" % ip_address
        result = self.cmd(cmd)
        if "bytes from %s: icmp_seq" % ip_address in result:
            return True
        return False

    def find_mac(self, mac_address):

        mac_addresses = []

        cmd = "show fdb %s | i %s" % (mac_address, mac_address)
        result = self.cmd(cmd)
        for l in result.splitlines():
            if l[:17] == mac_address:
                mac = Mac(address=mac_address)
                parts = l.strip().split()
                vlan_concat = parts[1]
                vlan_pattern = re.compile("\([0-9]{1,4}\)")
                group = vlan_pattern.search(vlan_concat)
                mac.vlan_id = int(group.group()[1:-1])
                mac.vlan_name = vlan_concat[0:-6]
                mac_addresses.append(mac)

                interface = self.get_interface(parts[-1])

                mac.interface = interface

        return mac_addresses
