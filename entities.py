class Interface:
    def __init__(self, name=None, description=None):
        self.name = name
        self.description = description

    def __str__(self):
        return "%s (%s)" % (self.name, self.description)


class Vlan:
    def __init__(self, tag=None, name=None):
        self.tag = tag
        self.name = name

    def __str__(self):
        return "Vlan(tag=%s, name='%s')" % (self.tag, self.name)

    def __repr__(self):
        return "Vlan(tag=%s, name='%s')" % (self.tag, self.name)


class Mac:
    def __init__(self, address=None, vlan_id=None, vlan_name=None, interface=None):
        self.address = address
        self.vlan_id = vlan_id
        self.vlan_name = vlan_name
        self.interface = interface

    def __str__(self):
        return "%s %s (%s) %s" % (self.address, self.vlan_id, self.vlan_name, self.interface)

    def __repr__(self):
        return self.__str__()
