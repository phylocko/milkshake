import requests as r
from models import Device
from threading import Thread


def path_finder(source_bundle, target_bundle, tag, name, account):
    url = 'https://hostel.ihome.ru/api/?page=path_finder&source={source}&target={target}'.format(source=source_bundle,
                                                                                                 target=target_bundle)
    print('Fetching %s...' % url)
    path = r.get(url).json()

    print('Points:\t%s, %s' % (path['source_bundle'], path['target_bundle']))
    print('Service:\tvlan %s, name %s' % (tag, name))

    devices = path['path'][0]

    for device in devices:
        if device['type'] == 'switch':
            print(device['device'], device['ports'])
        else:
            print(device['device'], device['params'])



    go = input('Type "y" if you\'re sure: ')
    if not go == 'y':
        print('Cancelled by user')
        return

    for device in devices:
        d = Device(device['device'], account)
        if device['type'] == 'switch':
            pass
            d.put_vlan(tag, name, device['ports'])
        elif device['type'] == 'router':
            d.make_ccc(tag,
                       name,
                       device['params']['interface'],
                       device['params']['remote_address'])

    go = input('Ready to check l2channel? ')
    if go == 'y':
        edges = [devices[0]['device'], devices[-1]['device']]
        print(edges)
        test = check_l2channel(edges, tag, account)
        if test:
            print('Channel OK!')
        else:
            print('Channel is not working')





def assign_test_ip(device: str, vlan_id: int, address: str, account):
    device = Device(device, account)
    vlan = device.get_vlan(vlan_id)
    device.assign_vlan_ip(vlan, address)


def check_l2_service(service_id, account):
    url = 'https://hostel.ihome.ru/api/?page=service&action=get&id=%s' % service_id

    service = r.get(url).json()

    if not len(service['ports']) == 2:
        raise ValueError("Number of ports is not 2")

    if len(service['vlans']) > 1:
        raise ValueError("Service has more than one vlan")

    devices = [x.get('device') for x in service['ports']]
    vlan_id = service['vlans'][0]['tag']

    return check_l2channel(devices, vlan_id, account)


def check_l2channel(devices, vlan_id, account):
    pingable = False

    first_device = Device(devices[0], account)
    first_vlan = first_device.get_vlan(vlan_id)

    second_device = Device(devices[1], account)
    second_vlan = second_device.get_vlan(vlan_id)

    if not first_device.vlan_has_ip(first_vlan) and not second_device.vlan_has_ip(second_vlan):
        first_device.assign_vlan_ip(first_vlan, "192.58.14.1/30")
        second_device.assign_vlan_ip(second_vlan, "192.58.14.2/30")

        pingable = first_device.is_address_reachable("192.58.14.2")

        first_device.remove_vlan_ip(first_vlan)
        second_device.remove_vlan_ip(second_vlan)
    else:
        raise ValueError('Vlan %s has IP address' % vlan_id)

    return pingable


def find_mac(device_name, account, mac_address, mac_dump):
    device = Device(device_name, account)
    try:
        mac_addresses = device.find_mac(mac_address)
    except:
        print(" -> Unable to start command on %s" % device_name)
        return

    if not mac_addresses:
        return

    for mac in mac_addresses:
        mac_dump.append({"device": device_name, "mac": mac})


def find_mac_address(mac_address, devices, account):
    mac_dump = []
    threads = []
    for device in devices:
        threads.append(Thread(target=find_mac, args=(device['name'], account, mac_address, mac_dump)))
        threads[-1].start()
    for t in threads:
        t.join()

    return mac_dump
